<?php
/**
 * Registration model
 */

namespace App\Model;

use Spipu\Html2Pdf\Html2Pdf;
use Nette\Mail\Message;
use Nette\Mail\SendmailMailer;
use Nette;

class RegistrationMdl extends Nette\Object {

    use Nette\SmartObject;

    /**
     * @var Nette\Database\Context
     */
    private $database;

    public function __construct(Nette\Database\Context $database) {
        $this->database = $database;
    }

    /**
     * 
     * @param void $data
     * @return Nette\Database\Table\ActiveRow
     */
    public function insert($data) {
        $registration = $this->database->table('registrations')->insert($data);
        return $registration;
    }

    /**
     * create pdf
     * 
     * @param Nette\Database\Table\ActiveRow $registration
     * @return binary
     */
    public function createPdf(Nette\Database\Table\ActiveRow $registration) {

        $html2pdf = new Html2Pdf('P', 'A4', 'en', true, 'UTF-8');
        $html = '<h1>Registrace</h1>';
        $html.='<table width="100%">'
                . '<tr>'
                . '<td><strong>Jmeno:</strong></td>'
                . '<td>' . $registration['name'] . ' ' . $registration['surname'] . '</td>'
                . '</tr><tr>'
                . '<td><strong>Email:</strong></td>'
                . '<td>' . $registration['email'] . '</td>'
                . '</tr><tr>'
                . '<td><strong>Adresa:</strong></td>'
                . '<td>' . $registration['street'] . '</td>'
                . '</tr><tr>'
                . '<td><strong>Mesto:</strong></td>'
                . '<td>' . $registration['city'] . '</td>'
                . '</tr><tr>'
                . '<td><strong>PSC:</strong></td>'
                . '<td>' . $registration['zip'] . '</td>'
                . '</tr>'
                . '</table>';
        $html2pdf->writeHTML($html);
        $pdf = $html2pdf->output('registrace.pdf', 'S');
        return $pdf;
    }

    /**
     * send email
     * 
     * @param Nette\Database\Table\ActiveRow $registration
     */
    public function sendMail(Nette\Database\Table\ActiveRow $registration) {
        $pdf = $this->createPdf($registration);
        $mail = new Message;
        $mail->setFrom('Spagi <jstrnadel@gmail.com>')
                ->addTo($registration['email'])
                ->setSubject('Registrace')
                ->setBody("Dobrý den,\nvaše registrace byla přijata.");
        $mail->addAttachment('registrace.pdf', $pdf);

        $mailer = new SendmailMailer;
        $mailer->send($mail);
    }

}
