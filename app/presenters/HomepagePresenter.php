<?php

namespace App\Presenters;

use Tomaj\Form\Renderer\BootstrapRenderer;
use App\Model\RegistrationMdl;
use Nette;

class HomepagePresenter extends Nette\Application\UI\Presenter {

    /** @var \App\Model\RegistrationMdl @inject */
    public $registrationMdl;

    public function renderDefault() {
        $form = $this->getComponent('registrForm');
    }

    protected function createComponentRegistrForm() {
        $form = new Nette\Application\UI\Form();
        $form->setRenderer(new BootstrapRenderer);

        $form->addGroup('Osobni data');
        $form->addText('name', 'Jméno')->setRequired('Zadejte jmeno');
        $form->addText('surname', 'Příjmení')->setRequired('Zadejte prijmeni');
        $form->addEmail('email', 'Email')->setRequired('Zadejte email')->addRule(Nette\Forms\Form::EMAIL, 'Zadejte platnou mailovou adresu');

        $form->addGroup('Kontaktni adresa');
        $form->addText('street', 'Adresa');
        $form->addText('city', 'Mesto');
        $form->addText('zip', 'PSČ:')
                ->setRequired('PSC je povine')
                ->addRule(Nette\Forms\Form::PATTERN, 'PSČ musí mít 5 číslic', '([0-9]\s*){5}');
        $form->addSubmit("action", "Odeslat")->setAttribute('class', 'btn btn-primary');
        $form->onSuccess[] = array($this, 'RegistrFormSucceeded');
        $form->onError[] = array($this, 'onRegistrFormError');
        return $form;
    }

    public function registrFormSucceeded(Nette\Application\UI\Form $form, $values) {
        $registration = $this->registrationMdl->insert($values);
        if ($registration) {
            $pdf = $this->registrationMdl->sendMail($registration);
            $this->flashMessage('Byl jste úspěšně registrován.');
            $this->redirect('this');
        }
    }

    public function onRegistrFormError(Nette\Application\UI\Form $form) {
        foreach ($form->errors as $error) {
            $this->flashMessage($error);
        }
    }

}
